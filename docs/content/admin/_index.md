+++
title = "管理员设置"
date = 2019-03-07T16:34:18+08:00
weight = 5
chapter = true
pre = "<b>01. </b>"
+++

### 管理员

# 第一次登录
默认账号 admin，密码 admin。

![Accpet the insecure SSL](images/Screenshot_2019-03-07_17-17-58.png)

# 创建另一个管理员账号。

# 修改管理员密码
Jumpserver 默认的管理员账号 admin 是无法改名或删除的，所以，设置一个复杂的高强度密码是必须的。

登录后在右上角点击 Administrator 菜单，再点击“个人信息”

![admin_info](images/Screenshot_2019-03-08_10-24-46.png)

点击“更改密码:”右边的“更新”按钮。

![admin_edit](images/Screenshot_2019-03-08_10-34-46.png)

设置一个 12 位长度或以上，包含数字、大小写字母和特殊符号的密码，尤其是当你的 Jumpserver 需要通过外网访问时。

***注意要保存好这个密码。***

![password_edit](images/Screenshot_2019-03-08_11-05-08.png)

网上也有自动生成密码的工具页面，例如 [密码生成器 Lastpass](https://www.lastpass.com/zh/password-generator)
