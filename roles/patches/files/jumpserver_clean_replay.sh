#!/bin/sh
## 这个脚本用于监控磁盘空间，如果超过百分比则清理最早的 1 个回放记录目录，请谨慎启用。
## 可以手动执行多次，直到使用率低于预警线。

# 设置磁盘使用警戒率
DISK_USED_TOP=85

# 获取 / 的使用率
DISK_USED=$(df -h | egrep /$ | awk '{print $5 }' | sed 's/%//g')

if [[ $DISK_USED > DISK_USED_TOP ]]; then
  echo "Root filesystem is in used over $DISK_USED%"
  ls -tr /opt/jumpserver/data/media/replay/ | head -n1 | xargs rm -rf
fi
