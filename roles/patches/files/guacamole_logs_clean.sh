#!/bin/sh

ls /config/tomcat8/logs/localhost* | grep -v $(date +%F) | xargs rm -f
ls /config/tomcat8/logs/manager*.log | grep -v $(date +%F) | xargs rm -f
ls /config/tomcat8/logs/host-manager*.log | grep -v $(date +%F) | xargs rm -f
ls /config/tomcat8/logs/catalina*.log | grep -v $(date +%F) | xargs rm -f
ls /config/guacamole/data/log/history/* | grep -v $(date +%Y%m%d) | xargs rm -f
