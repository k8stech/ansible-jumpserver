# ansible-jumpserver

#### 介绍

使用 [Ansible](https://www.ansible.com) 自动化部署软堡垒机 [jumpserver](https://github.com/jumpserver/jumpserver)。
实现以下功能：

1. 支持 All In One 部署，也支持使用外部 Redis/MySQL。
1. 启用 Yum/Apt 的自动更新功能，确保堡垒机系统的缺陷漏洞尽量少。
1. 自动备份数据库，备份数据在 /var/backup/db 目录下。
1. 自带监控服务 monit，实现基本的服务自修复。

---

#### 软件架构

目前只支持单机部署。Redis 和数据库可以使用外部连接。

在以下环境及工具测试通过

| 系统/软件 | 版本   |
| --------- | ------ |
| Ubuntu    | xenial |
| CentOS    | 7      |
| Ansible   | 2.7.5  |
| python    | 3.6    |

---

#### 安装教程

##### 准备前

1. 在控制机上获取部署代码。
```
git clone --depth 1 https://gitee.com/bottlelee/ansible-jumpserver.git
```

2. 在控制机上安装 ansible。
```
chmod +x bootstrap-ansible.sh
sudo -H ./bootstrap-ansible.sh
```

3. 复制 sample inventory，使用你需要的名称（例如 stage）
```
cp -R inventories/sample inventories/stage
```

##### 修改 hosts 和 group_vars

编辑 `inventories/stage/hosts.ini`，就修改 jms 的连接信息就好了。


编辑 `inventories/stage/group_vars/all/all.yml`，主要是 jms_zone 的名称，用于识别目标区(例如不同的机房或网域)。

##### 初次部署

```
ansible-playbook -i inventories/stage/hosts.ini play-all.yml
```

##### 分步部署

1. 在 vars 目录下找到同名变量文件，例如 00-init.yml 对应的变量配置在 `inventories/stage/group_vars/all/init.yml` 下，按实际需求修改。
1. 用 ansible-playbook 命令执行对应的 playbook 即可。例如 `ansible-playbook -i inventories/stage/hosts.ini 00-init.yml`
1. 对变量和模板的内容不理解的，请参考 [官方文档](http://docs.jumpserver.org/zh/docs/index.html) 的说明。

---

#### 使用说明

1. 部署过程中生成的密码都存放在 `inventories/stage/credentials` 目录下。
1. [Jumpserver 管理文档](http://docs.jumpserver.org/zh/docs/admin_guide.html)
1. [Jumpserver 用户使用文档](http://docs.jumpserver.org/zh/docs/user_guide.html)
1. 部署的服务都使用 systemd 进行管理，如 `systemctl restart jumpserver` 或 `service jumpserver restart`

---

#### 生产环境适配

1. 跳过 01-redis.yml 和 02-mysql.yml 的执行。本项目部署的只是单点数据库，不具备高可用，切勿使用到生产环境。
1. 修改 `inventories/stage/group_vars/all/jumpserver.yml` 里“自定义 redis”和“自定义 mysql”两部分的内容。
1. 这样你就可以部署多个 jumpserver+coco+guacamole+luna 的服务器节点，它们共享同一个数据库。

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

---

#### 我的其它项目

[自动化部署 ELK](https://gitee.com/bottlelee/ansible-elk-deploy)
